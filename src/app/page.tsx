export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1 className="text-3xl dark:text-indigo-300 text-indigo-700">
        Hello World!
      </h1>
    </main>
  );
}
